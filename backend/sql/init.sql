-- Enable pgcrypto extension if not already enabled
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

-- Grant privileges
-- GRANT ALL PRIVILEGES ON DATABASE moodtracker TO laura;

DROP TABLE IF EXISTS tracker_activities;
DROP TABLE IF EXISTS trackers;
DROP TABLE IF EXISTS settings;
DROP TABLE IF EXISTS activities;
DROP TABLE IF EXISTS invites;
DROP TABLE IF EXISTS users;

CREATE TABLE users (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    username VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(255) NOT NULL
);

CREATE TABLE activities (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    label VARCHAR(255),
    status VARCHAR(255),
    user_id UUID REFERENCES users(id)
);

CREATE TABLE invites (
    status VARCHAR(255),
    sender_id UUID REFERENCES users(id),
    recipient_id UUID REFERENCES users(id),
    PRIMARY KEY (sender_id, recipient_id)
);

CREATE TABLE trackers (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    day DATE,
    rating INT CHECK (rating >= 1 AND rating <= 5),
    comment TEXT, 
    picture VARCHAR(255),
    user_id UUID REFERENCES users(id)
);

CREATE TABLE tracker_activities (
    tracker_id UUID REFERENCES trackers(id),
    activity_id UUID REFERENCES activities(id),
    PRIMARY KEY (tracker_id, activity_id)
);

CREATE TABLE settings (
    theme VARCHAR(255),
    user_id UUID REFERENCES users(id),
    PRIMARY KEY (user_id)
);

-- Fixtures for users table
INSERT INTO users (username, email, password, role)
VALUES 
    ('laura', 'laura@mail.com', 'password123', 'user'), 
    ('alexis', 'alexis@mail.com', 'password123', 'user'),
    ('lina', 'lina@mail.com', 'password123', 'user'),
    ('julie', 'julie@mail.com', 'password123', 'user');

-- Fixtures for activities table
INSERT INTO activities (label, status, user_id)
VALUES 
    ('Roller skate', 'visible', (SELECT id FROM users WHERE username = 'laura')),
    ('Cook', 'visible', (SELECT id FROM users WHERE username = 'laura')),
    ('Drums', 'archived', (SELECT id FROM users WHERE username = 'laura')),
    ('Gaming', 'visible', (SELECT id FROM users WHERE username = 'alexis')),
    ('Run', 'visible', (SELECT id FROM users WHERE username = 'lina')),
    ('Study', 'visible', (SELECT id FROM users WHERE username = 'julie'));

-- Fixtures for invites table
INSERT INTO invites (status, sender_id, recipient_id)
VALUES 
    ('accepted', 
     (SELECT id FROM users WHERE username = 'laura'), 
     (SELECT id FROM users WHERE username = 'alexis')),
    ('accepted', 
     (SELECT id FROM users WHERE username = 'alexis'), 
     (SELECT id FROM users WHERE username = 'lina')),
    ('rejected', 
     (SELECT id FROM users WHERE username = 'lina'), 
     (SELECT id FROM users WHERE username = 'julie')),
    ('pending', 
     (SELECT id FROM users WHERE username = 'julie'), 
     (SELECT id FROM users WHERE username = 'laura'));

-- Fixtures for trackers table
INSERT INTO trackers (day, rating, comment, picture, user_id)
VALUES 
    ('2024-07-28', 5, 'Feeling great!', 'path/to/picture.jpg', (SELECT id FROM users WHERE username = 'laura')),
    ('2024-07-27', 4, 'Good day overall', NULL, (SELECT id FROM users WHERE username = 'alexis')),
    ('2024-07-26', 3, 'Average day', 'path/to/picture2.jpg', (SELECT id FROM users WHERE username = 'lina')),
    ('2024-07-25', 2, 'Not the best day', NULL, (SELECT id FROM users WHERE username = 'julie')),
    ('2024-07-24', 1, 'Feeling down', 'path/to/picture3.jpg', (SELECT id FROM users WHERE username = 'laura')),
    ('2024-07-23', 5, 'Fantastic day!', 'path/to/picture4.jpg', (SELECT id FROM users WHERE username = 'alexis'));

-- Fixtures for tracker_activities table
INSERT INTO tracker_activities (tracker_id, activity_id)
VALUES 
    (
        (SELECT id FROM trackers WHERE user_id = (SELECT id FROM users WHERE username = 'laura') AND day = '2024-07-28'),
        (SELECT id FROM activities WHERE label = 'Roller skate' AND user_id = (SELECT id FROM users WHERE username = 'laura'))
    ),
    (
        (SELECT id FROM trackers WHERE user_id = (SELECT id FROM users WHERE username = 'laura') AND day = '2024-07-28'),
        (SELECT id FROM activities WHERE label = 'Cook' AND user_id = (SELECT id FROM users WHERE username = 'laura'))
    ),
    (
        (SELECT id FROM trackers WHERE user_id = (SELECT id FROM users WHERE username = 'laura') AND day = '2024-07-28'),
        (SELECT id FROM activities WHERE label = 'Drums' AND user_id = (SELECT id FROM users WHERE username = 'laura'))
    ),
    (
        (SELECT id FROM trackers WHERE user_id = (SELECT id FROM users WHERE username = 'alexis') AND day = '2024-07-27'),
        (SELECT id FROM activities WHERE label = 'Gaming' AND user_id = (SELECT id FROM users WHERE username = 'alexis'))
    ),
    (
        (SELECT id FROM trackers WHERE user_id = (SELECT id FROM users WHERE username = 'lina') AND day = '2024-07-26'),
        (SELECT id FROM activities WHERE label = 'Run' AND user_id = (SELECT id FROM users WHERE username = 'lina'))
    ),
    (
        (SELECT id FROM trackers WHERE user_id = (SELECT id FROM users WHERE username = 'julie') AND day = '2024-07-25'),
        (SELECT id FROM activities WHERE label = 'Study' AND user_id = (SELECT id FROM users WHERE username = 'julie'))
    ),
    (
        (SELECT id FROM trackers WHERE user_id = (SELECT id FROM users WHERE username = 'laura') AND day = '2024-07-24'),
        (SELECT id FROM activities WHERE label = 'Roller skate' AND user_id = (SELECT id FROM users WHERE username = 'laura'))
    ),
    (
        (SELECT id FROM trackers WHERE user_id = (SELECT id FROM users WHERE username = 'alexis') AND day = '2024-07-23'),
        (SELECT id FROM activities WHERE label = 'Gaming' AND user_id = (SELECT id FROM users WHERE username = 'alexis'))
    );


-- Fixtures for settings table
INSERT INTO settings (theme, user_id)
VALUES 
    ('dark', (SELECT id FROM users WHERE username = 'laura')),
    ('light', (SELECT id FROM users WHERE username = 'alexis')),
    ('dark', (SELECT id FROM users WHERE username = 'lina')),
    ('light', (SELECT id FROM users WHERE username = 'julie'));



-- Tests requests 
SELECT 
    t.day,
    t.rating,
    t.comment,
    t.picture,
    string_agg(a.label, ', ') AS activities
FROM 
    trackers t
JOIN 
    users u ON t.user_id = u.id
JOIN 
    tracker_activities ta ON t.id = ta.tracker_id
JOIN 
    activities a ON ta.activity_id = a.id
WHERE 
    u.username = 'laura'
GROUP BY 
    t.id, t.day, t.rating, t.comment, t.picture
ORDER BY 
    t.day DESC;


SELECT 
    a.label,
    a.status
FROM 
    activities a
JOIN 
    users u ON a.user_id = u.id
WHERE 
    u.username = 'laura';

 SELECT 
    u.username AS friend_name
FROM 
    invites i
JOIN 
    users u ON i.recipient_id = u.id OR i.sender_id = u.id
WHERE 
    (i.sender_id = (SELECT id FROM users WHERE username = 'alexis') OR 
     i.recipient_id = (SELECT id FROM users WHERE username = 'alexis'))
    AND i.status = 'accepted'
    AND u.username != 'alexis';
